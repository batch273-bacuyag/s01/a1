
/*
1. How do you create arrays in JS? using square brackets notation
const myArray = [1, 2, 3];

2. How do you access the first character of an array? use the index 0
const firstElement = myArray[0]

3. How do you access the last character of an array? use the index array.length - 1
const lastElement = myArray[myArray.length - 1]

4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
indexOf()

5. What array method loops over all elements of an array, performing a user-defined function on each iteration? forEach()

6. What array method creates a new array with elements obtained from a user-defined function? map()

7. What array method checks if all its elements satisfy a given condition? every()

8. What array method checks if at least one of its elements satisfies a given condition? some()

9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged. False

10.True or False: array.slice() copies elements from original array and returns them as a new array. True
*/

//function coding
//1. 

const students = ["John", "Joe", "Jane", "Jessie"];

function addToEnd(arr, str) {
  	if (typeof str !== "string") {
    	return "error - can only add strings to an array";
  	}
  	arr.push(str);
  	return arr;
}

//2. 

function addToStart(arr, str) {
  	if (typeof str !== "string") {
    	return "error - can only add strings to an array";
  	}
  	arr.unshift(str);
  	return arr;
}


//3.

function elementChecker(arr, val) {
  	if (arr.length === 0) {
    	return "error - passed in array is empty";
  	}
  	return arr.includes(val);
}


//4.

function checkAllStringsEnding(arr, char) {
  	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}
  	if (!arr.every((el) => typeof el === "string")) {
    	return "error - all array elements must be strings";
  	}
  	if (typeof char !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}
  	if (char.length !== 1) {
    	return "error - 2nd argument must be a single character";
  	}
  	return arr.every((el) => el.endsWith(char));
}

//5.

function stringLengthSorter(arr) {
  //all elements in the array are strings
  	const allStrings = arr.every(elem => typeof elem === 'string');
  	if (!allStrings) {
    	return "error - all array elements must be strings";
  	}

  // sort array by string length in ascending order
  	const sortedArr = arr.sort((a, b) => a.length - b.length);
  
  	return sortedArr;
}

//6. 

function startsWithCounter(arr, char) {
  // is empty
  	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}

  // all array elements are strings
  	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      	return "error - all array elements must be strings";
    	}
  	}

  // char is a string
  	if (typeof char !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  
    // char is with length of 1
  	if (char.length !== 1) {
    	return "error - 2nd argument must be a single character";
  	}

  // Count number of elements that start with the char argument
  	let count = 0;
  	for (let i = 0; i < arr.length; i++) {
    	if (arr[i].charAt(0).toLowerCase() === char.toLowerCase()) {
      		count++;
    	}
  	}
  	return count;
}


//7.
function likeFinder(arr, str) {
  //is empty
  	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}

  //all elements are strings
  	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      	return "error - all array elements must be strings";
    	}
  	}

  //second argument is a string
  	if (typeof str !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  // new array
  	let result = [];

  // Loop through the array and check if each element contains the string
  	for (let i = 0; i < arr.length; i++) {
    	if (arr[i].toLowerCase().includes(str.toLowerCase())) {
      	result.push(arr[i]);
    	}
  	}

  	return result;
}

//8.

function randomPicker(array) {
  	if (array.length === 0) {
    	return "error - array must NOT be empty";
  	}
  	const randomIndex = Math.floor(Math.random() * array.length);
  	return array[randomIndex];
}